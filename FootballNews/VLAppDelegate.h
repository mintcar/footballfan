//
//  VLAppDelegate.h
//  FootballNews
//
//  Created by Vlad Valt on 6/4/13.
//  Copyright (c) 2013 Vlad Valt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

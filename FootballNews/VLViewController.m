//
//  VLViewController.m
//  FootballNews
//
//  Created by Vlad Valt on 6/4/13.
//  Copyright (c) 2013 Vlad Valt. All rights reserved.
//
#import "VLTeamData.h"
#import "VLViewController.h"

@interface VLViewController ()
@property (weak, nonatomic) IBOutlet UIScrollView *image;
@property (strong,nonatomic) UIImageView * imageView;
@property (weak, nonatomic) IBOutlet UITextView *teamInfo;
@property NSDictionary * map;
@property (weak, nonatomic) IBOutlet UIImageView *testView;

@end

@implementation VLViewController


-(void) resetImage{
    if(self.image){
        NSURL *imageURL = [[NSURL alloc] initWithString:@"http://www.bubblews.com/assets/images/news/1457110047_1369647405.jpg"];
        _image.contentSize=CGSizeZero;
        _imageView=nil;
        NSData *imageData = [[NSData alloc] initWithContentsOfURL:imageURL];
        UIImage *image = [[UIImage alloc] initWithData:imageData];
        if(image){
            self.image.contentSize = image.size;
            self.imageView.image = image;
            _testView.image = image;
            self.imageView.frame = CGRectMake(0, 0, image.size.width, image.size.height);
        }
    }
}

-(UIImageView *) imageView{
    if(!_imageView){
        _imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    }
    return _imageView;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self resetImage];
    [self.image addSubview:self.imageView];        
        
    NSMutableDictionary * dict;
    dict = [[NSMutableDictionary alloc] init];
    [dict setObject:@"info aa" forKey:@"MU"];
    if([_info.team_Name isEqualToString:@"MU"]){
        _teamInfo.text = @"FFF";
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}

@end

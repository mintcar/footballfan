//
//  VLViewController.h
//  FootballNews
//
//  Created by Vlad Valt on 6/4/13.
//  Copyright (c) 2013 Vlad Valt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VLTeamData.h"

@interface VLViewController : UIViewController

@property(nonatomic,strong) VLTeamData *info;

@end

//
//  VLCell.m
//  FootballNews
//
//  Created by Vlad Valt on 6/7/13.
//  Copyright (c) 2013 Vlad Valt. All rights reserved.
//

#import "VLCell.h"

@interface VLCell ()


@property int spacing;

@end


@implementation VLCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

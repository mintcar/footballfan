//
//  main.m
//  FootballNews
//
//  Created by Vlad Valt on 6/4/13.
//  Copyright (c) 2013 Vlad Valt. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "VLAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([VLAppDelegate class]));
    }
}
